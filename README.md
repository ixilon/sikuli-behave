# sikuli-behave

`sikuli-behave` is a dockerized testing framework containing a step library intended to allow users to easily write [Selenium](https://github.com/SeleniumHQ/selenium) and [SikuliX](http://sikulix.com/) tests with the [Behave](https://behave.readthedocs.io) testing framework.  

## Usage

```
docker run -it --rm \
  --env DISPLAY \
  --volume $HOME/.Xauthority:/root/.Xauthority \
  --volume /tmp/.X11-unix:/tmp/.X11-unix \
  --volume $PWD:$PWD \
  --workdir $PWD \
  ixilon/sikuli-behave behave
```

The running Docker container must have permissions to connect to the XServer. 

## Behave tests

Behave uses tests written in a natural language style ([Gherkin](https://cucumber.io/docs/gherkin/reference/) language), backed up by Python code (step implementations).

`sikuli-behave` step implementations must be written for Python language level 2.7.

For details about how to write Behave tests read [this tutorial](https://behave.readthedocs.io/en/latest/tutorial.html).

## Selenium tests

Selenium is for automating web applications for testing purposes.

### Initialize selenium webdriver in the environment 

`sikuli-behave` contains the [behave-webdriver](https://github.com/spyoungtech/behave-webdriver) step library.
Add this import statement to your `environment.py` to initialize `behave-webdriver` and integrate it with the `sikuli-behave` Docker image.

```
# features/environment.py
from sikuli_behave.environment import *
```

### Importing the step implementations

To use [step definitions](https://github.com/spyoungtech/behave-webdriver#list-of-step-definitions-) of the `behave-webdriver` step library add this import statement to your step implementation:

```
# features/steps/my_step.py
from behave_webdriver.steps import *
```

## SikuliX tests

SikuliX automates anything you see on the screen.
It uses image detection and text recognition (OCR) powered by [OpenCV](https://github.com/opencv/opencv) to identify and control GUI components.
This allows users to write end-to-end tests for desktop as well as web applications,
even if there is no easy access to internals or the source code of the application or web page.

### Importing the step implementations

`sikuli-behave` defines some useful steps for running SikuliX tests with Behave.
To use them, the `sikuli-behave` step library must be imported into your own step implementation file:

```
# features/steps/my_step.py
from sikuli_behave.steps import *
```

### When steps

* I (left|right)* clicked on the text "{text}"
* I (left|right)* clicked on the image "{image}"
* I typed "{text}" into the input field "{label}"

### Then steps

* I expect( not)* to see the text "{text}"
* I expect( not)* to see the image "{image}"

### Images location

Save images referenced by steps in the folder `features/steps/image` as PNG files.
The following step

```
Then I expect to see the image "foo"
```

is true if the image `features/steps/image/foo.png` is visible on the screen.

### Verification

If a test step fails, then a screenshot of the current web page is saved in the file `screenshot.png`.

## Interactive tests

If a step can't be implemented, then the user can confirm whether the step has been failed or not.

```
@then(u'I expect to hear a beep')
def step_impl(context):
    context.ask()
```

## Headless tests

Use headless mode if there is no XServer, for instance if `sikuli-behave` is to be started in a continuous integration pipeline.
The screen size can be changed by setting the environment variable `SCREEN`. By default the screen size has the value `1280x1024x24`.

```
docker run --rm -v ${PWD}:/data -w /data -e SCREEN=800x600x24 ixilon/sikuli-behave xvfb-wrapper behave --stop -c
```
