Feature: Website login with Sikuli

  As browser user
  I want to login to a protected web page
  so that I can access restricted content

  Background:
    Given I open the url "http://sut"

  @skip
  Scenario: Login form
     Then I expect to see the text "Sign in"
      And I expect to see the text "Username"
      And I expect to see the text "Password"
      And I expect to see the image "Cancel"
      And I expect to see the image "SignIn"

  Scenario: Access denied when credentials are missing
     When I clicked on the image "Cancel" 
     Then I expect to see the text "Unauthorized"

  @skip
  Scenario: Access allowed when correct credentials are entered
     When I typed "demo" into the input field "Username"
      And I typed "demo" into the input field "Password"
      And I clicked on the image "SignIn" 
     Then I expect to see the text "It works!"
