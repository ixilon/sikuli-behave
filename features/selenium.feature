Feature: Website login with Selenium

  As browser user
  I want to login to a protected web page
  so that I can access restricted content

  Background:
    Given I open the url "http://sut"

  @skip
  Scenario: Login form
     When a alertbox is opened
     Then I expect that a alertbox contains the text "Sign in"
      And I expect that a alertbox contains the text "Usernames"
      And I expect that a alertbox contains the text "Password"
