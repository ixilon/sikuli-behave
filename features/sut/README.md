# System under test (SUT)

The system under test is an initial Apache webserver which needs authentication.
Username and password are "demo".

## Build

```
docker build -t registry.gitlab.com/ixilon/sikuli-behave/sut features/sut
```

## Push to registry

```
docker login registry.gitlab.com
docker push registry.gitlab.com/ixilon/sikuli-behave/sut
```

## Run

```
docker run --rm --publish 80:80 registry.gitlab.com/ixilon/sikuli-behave/sut
```
