# -*- coding: UTF-8 -*-

"""This module may define code to run before and after certain events during testing."""

import shutil
from functools import partial
import behave_webdriver
from behave_webdriver.driver import ChromeOptions, FirefoxOptions
from behave_webdriver.fixtures import transformation_fixture, fixture_browser
from behave_webdriver.transformers import FormatTransformer
from behave.fixture import use_fixture
from org.sikuli.script import Screen


def get_driver(**kwargs):
    """Define webdriver and its options."""
    kwargs.setdefault('default_wait', 5)
    # pylint: disable=protected-access
    driver = behave_webdriver.utils._from_env(default_driver='Chrome')

    if driver == behave_webdriver.Chrome:
        opts = ChromeOptions()
        opts.add_argument("--disable-extensions")
        # opts.add_argument("--disable-gpu")
        opts.add_argument("--disable-dev-shm-usage")
        opts.add_argument("--no-sandbox")
        opts.add_argument("--start-maximized")
        opts.add_argument("--disable-infobars")
        kwargs['chrome_options'] = opts

    if driver == behave_webdriver.Firefox:
        opts = FirefoxOptions()
        opts.add_argument("--safe-mode")
        kwargs['firefox_options'] = opts

    return driver, kwargs


def before_all(context):
    """Run before the whole shooting match."""
    driver, kwargs = get_driver()
    context.BehaveDriver = partial(driver, **kwargs)
    use_fixture(fixture_browser, context, webdriver=driver, **kwargs)
    use_fixture(transformation_fixture, context, FormatTransformer,
                BASE_URL='http://localhost:8000', ALT_BASE_URL='http://127.0.0.1:8000')


def before_feature(context, feature):
    """Run before each feature file is exercised."""
    # pylint: disable=unused-argument
    if "skip" in feature.tags:
        feature.skip()
        return


def before_scenario(context, scenario):
    """Run before each scenario is run."""
    # pylint: disable=unused-argument
    if "skip" in scenario.effective_tags:
        scenario.skip()
        return


def before_step(context, step):
    """Support for interactive testing."""
    # pylint: disable=unused-argument

    def ask():
        """Ask the user for input if the step has been failed."""
        answer = raw_input("Please enter 'y' if this test step has been passed: ")
        if answer != 'y':
            raise AssertionError(answer)

    context.ask = ask


def after_step(context, step):
    """Save screenshot when test step failed."""
    # pylint: disable=unused-argument
    if step.status == 'failed':
        screen = Screen()
        filename = Screen.capture(screen).filename
        shutil.move(filename, 'screenshot.png')
