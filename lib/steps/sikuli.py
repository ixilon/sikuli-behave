# -*- coding: UTF-8 -*-

"""This module may define Sikuli dependent step definitions."""

import os.path
import time

from behave import when, then
from org.sikuli.script import Screen, Pattern


def retry(attempts=5):
    """
    This is a decorator which helps implementing an aspect oriented
    implementation of a retrying of certain steps which might fail sometimes.
    """

    def wrapper(function):
        # pylint: disable=missing-docstring

        def wrapped(*args, **kwargs):
            # pylint: disable=broad-except
            for attempt in range(attempts):
                try:
                    return function(*args, **kwargs)
                except Exception as exception:
                    assert attempt < attempts - 1, str(exception)
                time.sleep(1)

        return wrapped

    return wrapper


@when(u'I clicked on the text "{text}"')
@when(u'I left clicked on the text "{text}"')
def left_click_on_text(context, text):
    """Wait until a single word appears on the screen and then left click on it."""
    # pylint: disable=unused-argument
    wait_for_line(text).click()


@when(u'I right clicked on the text "{text}"')
def right_click_on_text(context, text):
    """Wait until a text line appears on the screen and then right click on the first word."""
    # pylint: disable=unused-argument
    wait_for_line(text).rightClick()


@when(u'I typed "{text}" into the input field "{label}"')
def type_input(context, text, label):
    """
    Wait until a text label appears on the screen
    and then type text into its associated textbox.
    """
    # pylint: disable=unused-argument
    screen = Screen()
    word = wait_for_line(label, screen)
    screen.paste(word.right(100), unicode(text))


@retry()
def wait_for_line(text, screen=Screen()):
    """
    Wait until text appears on the screen.
    This text may contain of several words,
    but must be in a single line.
    Click point is the center of the first word.
    """

    def find_line(text, screen):
        """Search for matching pattern. Don't retry here!"""
        splitted = text.split(' ', 1)
        # import pdb; pdb.set_trace()
        for word in screen.collectWords():
            if word.getText() != splitted[0]:
                continue
            # import pdb; pdb.set_trace()
            try:
                if len(splitted) > 1:
                    find_line(splitted[1], word.right().grow())
                return word
            except AssertionError:
                pass
        raise AssertionError("text not found: {text}".format(text=text))

    return find_line(text, screen)


@then(u'I expect to see the text "{text}"')
def see_text(context, text):
    """Wait until some text appears on the screen."""
    # pylint: disable=unused-argument
    wait_for_text(text)


@retry()
def wait_for_text(text, screen=Screen()):
    """Wait until some text appears on the screen."""
    assert text in screen.text(), "text not found"


@then(u'I expect not to see the text "{text}"')
def vanish_text(context, text):
    """Wait until some text disappears from the screen."""
    # pylint: disable=unused-argument
    wait_for_vanishing_text(text)


@retry()
def wait_for_vanishing_text(text, screen=Screen()):
    """Wait until some text disappears from the screen."""
    assert text not in screen.text(), "text found"


@then(u'I expect to see the image "{image}"')
def see_image(context, image):
    """Wait until some image appears on the screen."""
    # pylint: disable=unused-argument
    wait_for_image(image)


@when(u'I clicked on the image "{image}"')
@when(u'I left clicked on the image "{image}"')
def left_click_on_image(context, image):
    """Wait until an image appears on the screen and then left click on it."""
    # pylint: disable=unused-argument
    wait_for_image(image).click()


@when(u'I right clicked on the image "{image}"')
def right_click_on_image(context, image):
    """Wait until an image appears on the screen and then right click on it."""
    # pylint: disable=unused-argument
    wait_for_image(image).rightClick()


def wait_for_image(name, screen=Screen()):
    """Wait until an image appears on the screen."""
    path = os.path.join(os.getcwd(), 'features', 'steps', 'image', name)
    pattern = Pattern(path).similar(0.5)
    return wait_for_pattern(pattern, screen)


def wait_for_pattern(pattern, screen=Screen()):
    """Wait until a Sikulix pattern appears on the screen."""
    match = screen.exists(pattern, 5)
    assert match, "pattern not found"
    return match
