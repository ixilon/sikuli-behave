FROM ubuntu:18.04 AS builder

RUN apt-get --quiet --assume-yes update \
 && apt-get --quiet --assume-yes --no-install-recommends install \
    ca-certificates \
    gnupg \
    unzip \
    wget

RUN wget --quiet --output-document=- https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

RUN cd /usr/local/bin \
 && wget --quiet --output-document=docker-compose https://github.com/docker/compose/releases/download/1.25.3/run.sh \
 && chmod +x docker-compose

RUN cd /usr/local/bin \
 && wget --quiet https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip \
 && unzip chromedriver_linux64.zip \
 && chown root:root chromedriver \
 && chmod +x chromedriver \
 && rm chromedriver_linux64.zip

RUN cd /usr/local/bin \
 && wget --quiet https://github.com/mozilla/geckodriver/releases/download/v0.25.0/geckodriver-v0.25.0-linux64.tar.gz \
 && tar xzf geckodriver-v0.25.0-linux64.tar.gz \
 && rm geckodriver-v0.25.0-linux64.tar.gz

RUN cd /usr/local/lib \
 && wget --quiet https://launchpad.net/sikuli/sikulix/2.0.1/+download/sikulixapi-2.0.1.jar

RUN cd /usr/local/lib \
 && wget --quiet https://repo.maven.apache.org/maven2/org/python/jython-standalone/2.7.2/jython-standalone-2.7.2.jar

FROM python:2.7 AS wheels

COPY requirements.txt /
RUN pip download \
    --requirement requirements.txt \
    --dest wheels \
    --implementation jy \
    --python-version 27 \
    --only-binary=:all: \
    --platform linux

FROM ubuntu:18.04

ENV LANG en_US.UTF-8

COPY --from=builder /etc/apt/trusted.gpg /etc/apt/
COPY --from=builder /usr/local/bin/docker-compose /usr/local/bin/
COPY --from=builder /usr/local/bin/chromedriver /usr/local/bin/
COPY --from=builder /usr/local/bin/geckodriver /usr/local/bin/
COPY --from=builder /usr/local/lib/sikulixapi-2.0.1.jar /usr/local/lib/
COPY --from=builder /usr/local/lib/jython-standalone-2.7.2.jar /usr/local/lib/

RUN cd /usr/local/lib \
 && ln -s sikulixapi-2.0.1.jar sikulixapi.jar \
 && ln -s jython-standalone-2.7.2.jar jython-standalone.jar

COPY google-chrome.list /etc/apt/sources.list.d/

RUN apt-get --quiet --assume-yes update \
 && apt-get --quiet --assume-yes --no-install-recommends install \
    software-properties-common \
 && add-apt-repository ppa:alex-p/tesseract-ocr \
 && apt-get --quiet --assume-yes update \
 && apt-get --quiet --assume-yes --no-install-recommends install \
    docker.io \
    firefox \
    google-chrome-stable \
    language-pack-en \
    libgconf-2-4 \
    libopencv3.2-jni \
    libsikulixapi-jni \
    libtesseract-dev \
    libxi6 \
    locales \
    openjdk-11-jre \
    tesseract-ocr \
    tzdata \
    xauth \
    xvfb \
 && rm -rf /var/lib/apt/lists/* \
 && apt-get clean

COPY log4j.properties /usr/local/etc/
COPY jython /usr/local/bin/
COPY --from=wheels /wheels wheels

RUN jython -m ensurepip \
 && jython -m pip install wheels/*

# https://github.com/spyoungtech/behave-webdriver/issues/86
RUN sed --in-place --expression='s/\]\*)?/]+)?/g' /usr/local/Lib/site-packages/behave_webdriver/steps/*.py

COPY README.md setup.cfg setup.py sikuli-behave/
COPY lib/ sikuli-behave/lib/
RUN cd sikuli-behave && jython setup.py install

COPY xvfb-wrapper /usr/local/bin/

RUN cd /usr/lib \
 && ln -s /usr/lib/jni/libopencv_java320.so libopencv_java.so \
 && ldconfig
