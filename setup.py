# -*- coding: UTF-8 -*-

from setuptools import setup
from io import open

with open('README.md', 'rt', encoding='utf8') as f:
    readme = f.read()

setup(
    name='sikuli-behave',
    version='0.1.0',
    url='https://gitlab.com/ixilon/sikuli-behave/',
    license='MIT',
    author='Gerald Fiedler',
    author_email='gerald@ixilon.de',
    description='Sikuli step library for behave BDD testing',
    long_description=readme,
    package_dir={'sikuli_behave': 'lib'},
    packages=['sikuli_behave', 'sikuli_behave.steps'],
    platforms='any',
    classifiers=[
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Testing',
        'Topic :: Software Development :: Quality Assurance',
        'Topic :: Internet :: WWW/HTTP :: Browsers',
        'Topic :: Software Development :: Testing :: BDD',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
    ],
)
